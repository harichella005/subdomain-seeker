package main

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
	"sync"
	"time"
)

// Banner for this tool
const banner = "\x1b[32;40m" +
	`===============================================================================
||                                                                            ||
||                            Tool: Subdomain-Seeker                          ||
||                            Author: Hariharan                               ||
||                            Insta: Crypto_grapper_                          ||
||                                                                            ||
===============================================================================
` + "\x1b[32;40m"

// First get the domain from the user
func main() {
	fmt.Println(banner)
	fmt.Printf("\x1b[36;40mSubdomain Recon Started\n\n")
	// This if condition will check the argument if it's less than two without specifying the domain it will return Usages
	if len(os.Args) < 2 {
		fmt.Printf("Usage: go Subdomain-Seeker.go domain.com\n\n")
		fmt.Printf("OR\n\n")
		fmt.Printf("Usage: ./subdomain-seeker domain.com\n\n")
		return
	}

	if len(os.Args) == 2 && (os.Args[1] == "-h" || os.Args[1] == "-help") {
		Helper()
		return
	}

	domain := os.Args[1]

	startTime := time.Now()

	subdomains, err := readfilesline(domain)
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}

	// Create a new txt file with the domain name as the filename and store the data
	if err := writeDataToFile(domain, subdomains); err != nil {
		fmt.Println("Error writing data to file:", err)
	}

	elapsedTime := time.Since(startTime)
	fmt.Printf("\n")
	fmt.Printf("Total Subdomains discovered: %d\n\n", len(subdomains))
	fmt.Printf("Time taken: %s\n\n", elapsedTime)
}

func Helper() {
	helpMessage := `
	Usage: go Subdomain-Seeker.go domain.com
	
	This tool is used to discover subdomains for a given domain.
	
	Example:
	go Subdomain-Seeker.go example.com
	
	Options:
	-h, --help   Display this help message and exit.
	`
	fmt.Println(helpMessage)
}

func readfilesline(domain string) ([]string, error) {
	file, err := os.Open("wordlists.txt")
	if err != nil {
		return nil, fmt.Errorf("Error opening file: %v", err)
	}
	defer file.Close()

	// create scanner to read line by line
	scanner := bufio.NewScanner(file)

	// Create a wait group to synchronize goroutines
	var wg sync.WaitGroup

	// Limit the number of concurrent goroutines
	maxConcurrency := 200 // 20X faster than the original code
	semaphore := make(chan struct{}, maxConcurrency)

	// Create a mutex to protect the subdomains slice
	var mu sync.Mutex
	var subdomains []string

	// Read the file line by line
	for scanner.Scan() {
		line := scanner.Text()
		subdomain := line + "." + domain

		// Acquire a token from the semaphore to control the concurrency
		semaphore <- struct{}{}

		wg.Add(1)

		go func(subdomain string) {
			defer wg.Done()
			defer func() { <-semaphore }()

			url := "http://" + subdomain
			resp, err := http.Get(url)
			if err != nil {
				// fmt.Println("Error: ", err)
				return
			}
			defer resp.Body.Close()
			fmt.Println("\x1b[37;40m", subdomain, resp.StatusCode)

			// Increment the counter safely using the mutex
			mu.Lock()
			subdomains = append(subdomains, fmt.Sprintf("%s", subdomain))
			mu.Unlock()
		}(subdomain)
	}
	// Wait for all goroutines to finish
	wg.Wait()

	return subdomains, nil
}

func writeDataToFile(domain string, data []string) error {
	filename := domain + ".txt"
	file, err := os.Create(filename)
	if err != nil {
		return fmt.Errorf("Error creating file: %v", err)
	}
	defer file.Close()

	for _, d := range data {
		_, err := fmt.Fprintln(file, d)
		if err != nil {
			return fmt.Errorf("Error writing to file: %v", err)
		}
	}

	return nil
}
