# Subdomain-Seeker

![Banner Image](banner/subdomain-seeker-usage.png)


### Description: 

This tool is used to find the subdomains of a domain by automating the program, i used go lang for developing this tool 


## Installation step:

1. git clone https://gitlab.com/harichella005/subdomain-seeker.git
2. cd subdomain-seeker
3. go run Subdomain-Seeker.go domain.com
4. You can also use the build file ./subdomain-seeker domain.com

## Usage:

![Usage](banner/subdomain-seeker-run.png)